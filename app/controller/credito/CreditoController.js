Ext.define('app15.controller.credito.CreditoController', {
	extend : 'Ext.app.ViewController',

	alias : 'controller.credito',
	'nuevoUsuario' : function(button, e, eOpts) {
		var form = button.up('form');
		form.reset();
		var tab = button.up('#microcreditocreartab');
		var grid = tab.down('#creditogrid');
		try {
			grid.getStore().load();
			grid.getView().refresh();
			grid.getSelectionModel().deselectAll();
		} catch(ex) {
		}

	},
	'guardarUsuario' : function(button, e, eOpts) {
		var box = Ext.MessageBox.wait('Por favor espere.', 'Guardando');
		var form = button.up('form');
		var tab = button.up('#microcreditocreartab');

		if (form.isValid()) {

		//var record = form.getForm().getRecord();  
	
		var data = Ext.encode(form.getValues());
		var valor = JSON.parse(data).valor;
		var fecha = JSON.parse(data).fechaCredito;
		var cliente = JSON.parse(data).cliente;


		Ext.Ajax.request({
			url : 'http://localhost:8080/MicrocreditosPruebaJava/v1/Creditos/Creaar',
			method : 'GET',
			params : {
				valor : valor,
				fecha : fecha,
				cliente : cliente
			},
			callback : function(opt, success, respon) {
				box.hide();

				response = JSON.parse(respon.responseText)
				if (response!=undefined && response!=null && response!=""){

					if (response.error!=undefined){
						Ext.Msg.alert('Error', response.error);
					}else{
					form.reset();
					Ext.Msg.alert('Información', 'Credito guardado con exito');
					}
				}else{
					form.reset();
					Ext.Msg.alert('Error', 'Ha ocurrido un error en el servidor.');
				}
				return;
			}
		});



		}



		return;
	},
	'calcularProyeccion' : function(button, e, eOpts) {
		var box = Ext.MessageBox.wait('Por favor espere.', 'Calculando');
		var form = button.up('form');
		if (form.isValid()) {

		//var record = form.getForm().getRecord();  
	
		var data = Ext.encode(form.getValues());
		var valor = JSON.parse(data).valor;
		var fecha = JSON.parse(data).fechaCredito;
		Ext.MessageBox.minWidth = 350;
		Ext.Ajax.request({
			url : 'http://localhost:8080/MicrocreditosPruebaJava/v1/Creditos/Proyeccion',
			method : 'GET',
			params : {
				valor : valor,
				fecha : fecha
			},
			callback : function(opt, success, respon) {
				box.hide();

				response = JSON.parse(respon.responseText)
				if (response!=undefined && response!=null && response!=""){

					if (response.error!=undefined){
						Ext.Msg.alert('Error', response.error);
					}else{


					responseStr = '<tr><td  >Total a pagar :<td> '+accounting.formatMoney(response.totalPagar, "$", 3, ".", ",")+' </td>';
					responseStr += '<tr><td  >Porcentaje de interés :<td> '+response.porcentajeInteres+'% </td>';
					responseStr += '<tr><td  >Intereses :<td> '+accounting.formatMoney(response.interes, "$", 3, ".", ",")+' </td>';
					responseStr += '<tr><td  >Días de pago :<td> '+response.cantidadDias+' </td>';
					responseStr += '<tr><td  >Cobro diario :<td> '+accounting.formatMoney(response.cobrosDiarios, "$", 3, ".", ",")+' </td>';
					responseStr += '<tr><td  >Fecha de cierre: <td> '+response.fechaFin+' </td>';

					Ext.Msg.alert('Proyección', '<table class="table">'+responseStr+'</table>');
					}
				}else{
					Ext.Msg.alert('Error', 'Ha ocurrido un error en el servidor.');
				}
				return;
			}
		});



		}
		return;
	},
	'seleccionarGenero' : function(combo, record, options) {
		// nada por hacer aca
		return;
	},
	'seleccionar' : function(sender, record, index, options) {

		var grid = Ext.ComponentQuery.query('creditolist')[0];
        grid.getStore().load();

		var elems = Ext.ComponentQuery.query('#creditotab');
		var form = null;
		for (var i in elems) {
			try {
				form = elems[i].down('#creditoform');
			} catch(ex) {
			}
			if (Ext.isEmpty(form) != true) {
				break;
			}
		}

		var id = record.get("id");
		Ext.Ajax.request({
			url : 'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/'+id,
			callback : function(opt, success, respon) {

				form.loadRecord(record);

				return;
			}
		});
	}
});

