Ext.define('app15.controller.usuario.UsuarioController', {
	extend : 'Ext.app.ViewController',

	alias : 'controller.usuario',
	'nuevoUsuario' : function(button, e, eOpts) {
		var form = button.up('form');
		form.reset();
		var tab = button.up('#usuariotab');
		var grid = tab.down('#usuariogrid');
		try {
			grid.getStore().load();
			grid.getView().refresh();
			grid.getSelectionModel().deselectAll();
			console.log("1");
		} catch(ex) {
		}

	},
	'guardarUsuario' : function(button, e, eOpts) {
		var box = Ext.MessageBox.wait('Por favor espere.', 'Guardando');
		var form = button.up('form');
		var tab = button.up('#usuariotab');
		var grid = tab.down('#usuariogrid');
		if (form.isValid()) {

		//var record = form.getForm().getRecord();  
	
		var data = Ext.encode(form.getValues());
		var id = JSON.parse(data).id;
		var req = new XMLHttpRequest();
		var message= "";
		if (id!=undefined && id!=null && id!=""){
			req.open("PATCH", "http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/"+id, true);
			message = "Datos actualizado exitosamente.";
		}else{
			req.open("POST", "http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes", true);
			message = "Datos guardados exitosamente.";
		}
        
        req.setRequestHeader("Content-Type", "application/json");
        req.onreadystatechange = function () {


          if (req.readyState === 4 && req.status === 409) {
          	 	box.hide();
          		 let message_error = JSON.parse(req.response).errorMessage;
          		 if (message_error == "documento is unique"){
          		 	Ext.Msg.alert('Error', 'Ya existe un cliente con este documento.');
          		 }else{
          		 	Ext.Msg.alert('Error', 'Ha ocurrido un error en el servidor.');
          		 }
           		 
          }else{

        	if (id==undefined || id==null || id==""){
        		form.reset();
        	}
        	box.hide();
          	Ext.Msg.alert('Exito', message);
          	grid.getStore().load();
			grid.getView().refresh();
			grid.getSelectionModel().deselectAll();
			console.log("2");
          }
         
        };
        req.send(data);
    
		}
		return;
	},
	'eliminarUsuario' : function(button, e, eOpts) {
		console.log(button);
		console.log(e);
		console.log(eOpts);
		var grid = null;
		var elems = Ext.ComponentQuery.query('#usuariotab');
		var grid = null;
		var form = button.up('form');
		console.log(id);
		
		var form = button.up('form');
		var data = Ext.encode(form.getValues());
		var id = JSON.parse(data).id;

		for (var i in elems) {
			try {
				grid = elems[i].down('#usuariogrid');
			} catch(ex) {
			}
			if (Ext.isEmpty(grid) != true) {
				break;
			}
		}

		var form = null;
		for (var i in elems) {
			try {
				form = elems[i].down('#usuarioform');
			} catch(ex) {
			}
			if (Ext.isEmpty(form) != true) {
				break;
			}
		}

		console.log(form);

		Ext.Ajax.request({
			url : 'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/'+id,
			method : 'DELETE',
			callback : function(opt, success, respon) {
				if (grid != null) {
					try {
						grid.getStore().load();
					} catch(ex) {
					}
					try {
						grid.getView().refresh();
					} catch(ex) {
					}
				}
				if (form != null) {
					try {
						form.reset();
					} catch(ex) {
					}
				}

				var grid = Ext.ComponentQuery.query('usuariolist')[0];
                grid.getStore().load();

				Ext.Msg.alert('Exito', 'Usuario Eliminado con exito');
				return;
			}
		});

		return;
	},
	'seleccionarGenero' : function(combo, record, options) {
		// nada por hacer aca
		return;
	},
	'seleccionar' : function(sender, record, index, options) {

		var grid = Ext.ComponentQuery.query('usuariolist')[0];
        grid.getStore().load();

		var elems = Ext.ComponentQuery.query('#usuariotab');
		var form = null;
		for (var i in elems) {
			try {
				form = elems[i].down('#usuarioform');
			} catch(ex) {
			}
			if (Ext.isEmpty(form) != true) {
				break;
			}
		}

		var id = record.get("id");
		Ext.Ajax.request({
			url : 'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/'+id,
			callback : function(opt, success, respon) {

				form.loadRecord(record);

				return;
			}
		});
	}
});

