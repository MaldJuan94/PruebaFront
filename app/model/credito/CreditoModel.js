/**
 * @author guest
 */
Ext.define(
	'app15.model.credito.CreditoModel',
	{
		extend: 'Ext.data.Model',
		fields:
			[
				{
					name:'id',
					type:'int'
				},
				{
					name:'valor',
					type:'float'
				},

				{
					name:'pagado',
					type:'string',
					convert: function (newValue, model) {
						if (model.get('pagado')==true){
							return "SI";
						}else{
							return "NO";
						}
						
					}
				},


				{
					name:'fechaCredito',
					type:'string'
				},

				{
					name:'cliente',
					type:'int'
				},
				{name: 'ClienteNombre', mapping: 'cliente.primerNombre', type: 'string' },
				{name: 'ClienteApellido', mapping: 'cliente.primerApellido', type: 'string' },
				{name: 'ClienteCC', mapping: 'cliente.documento', type: 'string' },

				{
					name:'fecha',
					type:'string',
					convert: function (newValue, model) {
						var d = new Date(model.get('fechaFinCredito'));
						return d.toLocaleString();
					}
				},

				{
					name:'FullName',
					type:'string',
					convert: function (newValue, model) {
						return model.get('ClienteNombre')+" "+model.get('ClienteApellido');
					}
				},



				{
					name:'valorFormat',
					type:'string',
					convert: function (newValue, model) {
						return accounting.formatMoney(model.get('valorPrestamo'), "$", 3, ".", ",");
					}
				},

				{
					name:'cobroDiaFormat',
					type:'string',
					convert: function (newValue, model) {
						return accounting.formatMoney(model.get('cobroDia'), "$", 3, ".", ",");
					}
				},
				{
					name:'totalFormat',
					type:'string',
					convert: function (newValue, model) {
						return accounting.formatMoney(model.get('total'), "$", 3, ".", ",");
					}
				}

			]
		
	}
	
);


