/**
 * @author guest
 */
Ext.define(
	'app15.model.usuario.UsuarioModel',
	{
		extend: 'Ext.data.Model',
		fields:
			[
				{
					name:'id',
					type:'int'
				},
				{
					name:'primerNombre',
					type:'string'
				},
				{
					name:'segundoNombre',
					type:'string'
				},
				{
					name:'primerApellido',
					type:'string'
				},
				{
					name:'segundoApellido',
					type:'string'
				},
				{
					name:'documento',
					type:'string'
				},
				{
					name:'telefono',
					type:'string'
				},
				{
					name:'direccion',
					type:'string'
				},

				{
					name:'creado',
					type:'string'
				},		
				{
					name:'actualizado',
					type:'string'
				},	
				{
					name:'borrado',
					type:'string'
				},

				{
					name:'NombreCompleto',
					type:'string'
				},

			]
		
	}

);