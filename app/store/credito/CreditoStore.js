Ext.define(
	'app15.store.credito.CreditoStore',
	{
		extend: 'Ext.data.Store',
		model: 'app15.model.credito.CreditoModel',
		alias: 'store.credito',
		proxy: {
			type:'ajax',
			api:{
				create: 	'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes',
				read:		'http://localhost:8080/MicrocreditosPruebaJava/v1/Creditos',
				update: 	'source/index.php/usuario/update',
				destroy: 	'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/'
			},
			reader:{
				type: 'json',
				rootProperty: 'data'
			},
			writer: {
				type:'json',
				rootProperty: 'data',
				writeAllFields: true,
				encode: true,
				allowSingle: true
			}
		},
		autoLoad: true,
		autoSync: true
	}
);