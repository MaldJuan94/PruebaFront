Ext.define(
	'app15.store.usuario.UsuarioStore',
	{
		extend: 'Ext.data.Store',
		model: 'app15.model.usuario.UsuarioModel',
		alias: 'store.usuario',
		proxy: {
			type:'ajax',
			api:{
				create: 	'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes',
				read:		'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes',
				update: 	'source/index.php/usuario/update',
				destroy: 	'http://localhost:8080/MicrocreditosPruebaJava/v1/Clientes/'
			},
			reader:{
				type: 'json',
				rootProperty: 'data'
			},
			writer: {
				type:'json',
				rootProperty: 'data',
				writeAllFields: true,
				encode: true,
				allowSingle: true
			}
		},
		autoLoad: true,
		autoSync: true
	}
);