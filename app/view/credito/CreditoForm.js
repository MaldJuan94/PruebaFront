




Ext.define('app15.view.credito.CreditoForm', {
    extend: 'Ext.form.Panel',
    requires:[
    	'app15.controller.credito.CreditoController',
    ],
    xtype: 'creditoform',
    title: 'Formulario de Credito',
    bodyPadding: 10,
    controller: 'credito',
    frame: true,
      items: [
        {
            xtype: 'hiddenfield',
            name: 'id',
            fieldLabel: 'Id',
            labelWidth: 100,
            msgTarget: 'side',
            allowBlank: true,
            anchor: '100%'
        },



        {
            xtype: 'fieldcontainer',
            anchor: '100%',
            layout: 'hbox',
            bodyPadding:10,
            items:[
                {
                    xtype: 'numberfield',
                    name: 'valor',
                    fieldLabel: 'Valor',
                    labelWidth: 120,
                    msgTarget: 'side',
                    allowBlank: false,
                    flex: 1
                },
                {
                    xtype: 'menuseparator'
                }     
            ]
            
        },

            {
                    xtype: 'combobox',
                    name: 'cliente',
                    fieldLabel: 'Cliente',
                    labelWidth: 120,
                    msgTarget: 'side',
                    allowBlank: false,
                    flex: 1,

                    store: {
                        type: 'usuario'
                    },
                    queryMode: 'remote',
                    displayField: 'nombreCompleto',
                    valueField: 'id',
                }, 
        
        {
            xtype: 'datefield',
            name: 'fechaCredito',
            fieldLabel: 'Fecha Credito',
            format: 'd-m-Y',
            labelWidth: 120,
            msgTarget: 'side',
            allowBlank: false,
            anchor: '50%'
        }
        
    ],

    buttons: [
        {
            text: 'Guardar',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'guardarUsuario' 
		    }
        },
        {
            text: 'Nuevo',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'nuevoUsuario' 
		    }
        },
        {
            text: 'Calcular proyección ',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'calcularProyeccion' 
		    }
        }
    ]

});