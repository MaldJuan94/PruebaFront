Ext.define('app15.view.credito.CreditoGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'creditolist',

    requires: [
    	'app15.controller.credito.CreditoController',
        'app15.store.credito.CreditoStore'
    ],

    title: 'Creditos',
    
    controller: 'credito',

    store: {
        type: 'credito'
    },
    columns: [
        { 
            text: 'Id',  
            dataIndex: 'id' 
        },

        { 
            text: 'Cliente', 
            dataIndex: 'FullName', 
            flex: 1 
        },
        { 
            text: 'Documento', 
            dataIndex: 'ClienteCC', 
            flex: 1 
        },
        { 
            text: 'Valor Prestamo', 
            dataIndex: 'valorFormat', 
            flex: 1 
        },
        { 
        text: '% Interes', 
            dataIndex: 'interes', 
            flex: 1 
        },
        {
        text: 'Cantidad Dias', 
            dataIndex: 'dias', 
            flex: 1 
        },
        {
        text: 'Cobro por Dia', 
            dataIndex: 'cobroDiaFormat', 
            flex: 1 

        },
        {
        text: 'Total', 
            dataIndex: 'totalFormat', 
            flex: 1 
        },
        {
        text: 'Fecha Fin', 
            dataIndex: 'fecha', 
            flex: 1 
        },

        {
        text: 'Finalizado', 
            dataIndex: 'pagado', 
            flex: 1 
        },

    ],
});
