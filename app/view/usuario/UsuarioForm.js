
Ext.define('app15.view.usuario.UsuarioForm', {
    extend: 'Ext.form.Panel',
    requires:[
    	'app15.controller.usuario.UsuarioController',
    	'app15.store.usuario.GeneroStore'
    ],
    xtype: 'usuarioform',
    title: 'Formulario de Clientes',
    bodyPadding: 10,
    controller: 'usuario',
    frame: true,
      items: [
        {
            xtype: 'hiddenfield',
            name: 'id',
            fieldLabel: 'Id',
            labelWidth: 100,
            msgTarget: 'side',
            allowBlank: true,
            anchor: '100%'
        },
        {
        	xtype: 'fieldcontainer',
        	anchor: '100%',
        	layout: 'hbox',
        	bodyPadding:10,
        	items:[
		        {
		            xtype: 'textfield',
		            name: 'primerNombre',
		            fieldLabel: 'Primer Nombre',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex: 1
		        },
		        {
		        	xtype: 'menuseparator'
		        },
		        {
		            xtype: 'textfield',
		            name: 'segundoNombre',
		            fieldLabel: 'Segundo Nombre',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex: 1
		        }
        	]
        	
        },


        {
        	xtype: 'fieldcontainer',
        	anchor: '100%',
        	layout: 'hbox',
        	bodyPadding:10,
        	items:[
		        {
		            xtype: 'textfield',
		            name: 'primerApellido',
		            fieldLabel: 'Primer Apellido',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex: 1
		        },
		        {
		        	xtype: 'menuseparator'
		        },
		        {
		            xtype: 'textfield',
		            name: 'segundoApellido',
		            fieldLabel: 'Segundo Apellido',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex: 1
		        }
        	]
        	
        },

        {
        	xtype: 'fieldcontainer',
        	anchor: '100%',
        	layout: 'hbox',
        	bodyPadding:10,
        	items:[
				{
		            xtype: 'textfield',
		            name: 'documento',
		            fieldLabel: 'Documento',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex: 1
		        },
		        {
		        	xtype: 'menuseparator'
		        }     
		    ]
        	
        },
        
        // {
        //     xtype: 'datefield',
        //     name: 'nacimiento',
        //     fieldLabel: 'Nacimiento',
        //     format: 'Y-m-d',
        //     labelWidth: 100,
        //     msgTarget: 'side',
        //     allowBlank: false,
        //     anchor: '50%'
        // },
        {
        	xtype: 'fieldcontainer',
        	anchor: '100%',
        	layout: 'hbox',
        	bodyPadding:10,
        	items:[
		        {
		            xtype: 'textfield',
		            name: 'telefono',
		            fieldLabel: 'Telefono',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            flex:1
		        },
		        {
		        	xtype: 'menuseparator'
		        },
		        {
		            xtype: 'textfield',
		            name: 'direccion',
		            fieldLabel: 'Direccion',
		            labelWidth: 120,
		            msgTarget: 'side',
		            allowBlank: false,
		            // inputType: 'password', // propiedad importante para poner en modo password el textfield
		            flex:1
		        }		
        	]
        	
        }
        
    ],


    buttons: [
        {
            text: 'Guardar',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'guardarUsuario' 
		    }
        },
        {
            text: 'Nuevo',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'nuevoUsuario' 
		    }
        },
        {
            text: 'Eliminar',
            formBind: true, //only enabled once the form is valid
        	disabled: true,
        	scale: 'small',
            listeners: {
		        click: 'eliminarUsuario' 
		    }
        }
    ]

});