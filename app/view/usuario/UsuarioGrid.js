Ext.define('app15.view.usuario.UsuarioGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'usuariolist',

    requires: [
    	'app15.controller.usuario.UsuarioController',
        'app15.store.usuario.UsuarioStore'
    ],

    title: 'Clientes',
    
    controller: 'usuario',

    store: {
        type: 'usuario'
    },
    columns: [
        { 
            text: 'Id',  
            dataIndex: 'id' 
        },
        { 
            text: 'Primer Nombre', 
            dataIndex: 'primerNombre', 
            flex: 1 
        },
        { 
            text: 'Segundo Nombre', 
            dataIndex: 'segundoNombre', 
            flex: 1 
        },
        { 
            text: 'Primer Apellido', 
            dataIndex: 'primerApellido', 
            flex: 1 
        },
        { 
            text: 'Segundo Apellido', 
            dataIndex: 'segundoApellido', 
            flex: 1 
        },
        { 
            text: 'Documento', 
            dataIndex: 'documento', 
            flex: 1 
        },

        { 
            text: 'Telefono', 
            dataIndex: 'telefono', 
            flex: 1 
        },
        { 
            text: 'Direccion', 
            dataIndex: 'direccion', 
            flex: 1 
        },
    ],

    listeners: {
        select: 'seleccionar'
    }
});
